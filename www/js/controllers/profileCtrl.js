
App.controller('profileCtrl', function($scope,$ionicPopup, $timeout,$ionicModal,$http,$ionicLoading,$state,WebService,$rootScope,$ionicHistory) {

$scope.star = {};
$scope.rattingPopup = function() {

    $scope.comments = $rootScope.cmnts;
    $scope.star.val=$rootScope.ratng;
	$scope.data = {}
	myPopup = $ionicPopup.show({
   // template: '<div class="bms-close" ng-click="closePopup()"><i class="ion-close icon icon-left text-white"></i></div><div class="bms-rate-pop"><h1>REVIEW</h1><div class="bms-rate-pop-body"><h5>RATING</h5><form><span class="rating"><input type="radio" value="5" class="rating-input" id="rating-input-1-5" ng-model="star.val" name="val"><label for="rating-input-1-5" class="rating-star"></label><input type="radio" value="4" class="rating-input" id="rating-input-1-4" ng-model="star.val" name="val"> <label for="rating-input-1-4" class="rating-star"></label><input type="radio" value="3" class="rating-input" id="rating-input-1-3" ng-model="star.val" name="val"> <label for="rating-input-1-3" class="rating-star"></label><input type="radio" value="2" class="rating-input" id="rating-input-1-2" ng-model="star.val" name="val"> <label for="rating-input-1-2" class="rating-star"></label><input type="radio" value="1" class="rating-input" id="rating-input-1-1" ng-model="star.val" name="val"> <label for="rating-input-1-1" class="rating-star"></label></span></form><h5>COMMENTS</h5><textarea class="bms-pop-rate-input" ng-model="comments"></textarea><button class="bms-otp-btn" ng-click="closePopup();updateReview(comments)" >SUBMIT</button></div></div>',
   templateUrl: 'templates/ratereview.html',
   scope: $scope
	});
	myPopup.then(function(res) {

	});
};
$scope.closePopup = function() {
	myPopup.close();
};

/*EDIT-PROFILE*/


  $ionicModal.fromTemplateUrl('templates/edit.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.editModal = modal;
  });


  /*CHANGE-PASSWORD*/


  $ionicModal.fromTemplateUrl('templates/changepassword.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.changepasswordModal = modal;
  });



/* PROFILE VIEW
########################################################
--------------------------------------------------------*/
    var user_data=JSON.parse(localStorage.getItem('userData'));
    $rootScope.username=user_data.username;
    $rootScope.city=user_data.city;
    $rootScope.country=user_data.country;
    $rootScope.profile_pic=user_data.profile_pic;
    $rootScope.firstname=user_data.firstname;
    $rootScope.lastname=user_data.lastname;
    $rootScope.phone_no=user_data.phone_no;
    $rootScope.email_id=user_data.email_id;

/* EDIT PROFILE
########################################################
--------------------------------------------------------*/
// $scope.val={};
// $scope.editProfile=function(){

//   var userdatas=$scope.val;
//   //var profile_details = JSON.stringify(userdatas);
//   //alert(userdatas);
//   // if(profile_details == "{}") {
//   //     $scope.showMessage = true;
//   //     //$scope.class ="failure";
//   //     $scope.msg1="Please enter the values.";
//   //     $timeout(function() {
//   //       $scope.showMessage = false;
//   //     }, 3000);
//   // }
//   // else {
//     var user_data=JSON.parse(localStorage.getItem('userData'));
//     $rootScope.id_user=user_data.user_id;
//     var link = 'updateuser';
//     var post_data ={id:$rootScope.id_user,userdatas:userdatas};
//     WebService.show_loading();
//     var promise = WebService.send_data(link,post_data,"post");
//       promise.then(function(data){
//         //console.log(data);
//         $ionicLoading.hide();
//         if(data != ""){
//           $scope.showMessage = true;
//           $scope.msg2="Your profile updated successfully.";
//           setTimeout(function() {
//             window.location.reload();
//             $scope.editModal.hide();
//             $scope.showMessage = false;
//           }, 1000);
//         }
//       });
//     // }
//   }
$scope.val={};
//
$scope.editProfile=function(){
    var userdatas=$scope.val;
    var user_data=JSON.parse(localStorage.getItem('userData'));
    $rootScope.id_user=user_data.user_id;
    var link = 'updateuser';
    var post_data ={id:$rootScope.id_user,userdatas:userdatas};
    WebService.show_loading();
    var promise = WebService.send_data(link,post_data,"post");
        promise.then(function(data){
         $scope.editMsg = true;
         console.log(data);
         $ionicLoading.hide();
          if(data.status=='success'){
            $scope.message = data.message;
            $timeout(function() {
              // window.location.reload();
              $('.clear_input').prop('value', "");
              $scope.editModal.hide();
              $scope.editMsg = false;
            }, 2000);
          }
          if(data.status=='email_exist'){
            $scope.message = data.message1;
            $timeout(function() {
              $scope.editMsg = false;
            }, 5000);
          }
          if(data.status=='ph_exist') {
              $scope.message = data.message2;
              $timeout(function() {
                $scope.editMsg = false;
              }, 5000);
          }
          if(data.status=='validation_error') {
              $scope.message = data.valid_message;
              $timeout(function() {
                $scope.editMsg = false;
              }, 5000);
          }
        });
}
/* USER BOOKING SHOPS
########################################################
--------------------------------------------------------*/
$rootScope.mybooking=function(){

    var user_data=JSON.parse(localStorage.getItem('userData'));
    $rootScope.user_id=user_data.user_id;
    var link = 'userbookings';
    var post_data ={user_id:$rootScope.user_id};
    WebService.show_loading();
    var promise = WebService.send_data(link,post_data,"post");
        promise.then(function(data){
         //console.log(data);
          if(data!=""){
            $rootScope.msgbk=false;
            $rootScope.userbookings=data;
            $state.go('mybooking');
          }else{
            $state.go('mybooking');
            $rootScope.msgbk=true;
            $rootScope.msg="Sorry, No Result Found.";
          }
          $ionicLoading.hide();
        });
}
/* BOOKING HISTORY OF SINGLE SHOPS
########################################################
--------------------------------------------------------*/
$scope.bookinghistory=function(shopid, shopname, bookingdate,bookingtime, services,total,comments,rating,booking_id, price,booking_order_id){

    $rootScope.shopid= shopid;
    $rootScope.shopname= shopname;
    $rootScope.bookingdate= bookingdate;
    $rootScope.bookingtime= bookingtime;
    $rootScope.total=total;
    $rootScope.cmnts=comments;
    //alert($rootScope.cmnts);
    $rootScope.ratng=rating;
    var svc=services;
    var servc=svc.split("<=>");
    $rootScope.services=servc;
    $rootScope.booking_id=booking_id;
    $rootScope.price=price;
    // alert($rootScope.price);
    $rootScope.booking_order_id= booking_order_id;
    $state.go('bookinghistory');
}

/* UPDATE REVIEW
########################################################
--------------------------------------------------------*/
$scope.updateReview=function(comments){
    console.log(comments);

    var user_data=JSON.parse(localStorage.getItem('userData'));
    $rootScope.user_id=user_data.user_id;
    var link = 'ratingshop';
    var post_data ={shop_id:$rootScope.shopid,user_id:$rootScope.user_id,rating:$scope.star.val,comments:comments,booking_id:$rootScope.booking_id};
   // console.log(post_data);
    WebService.show_loading();
    var promise = WebService.send_data(link,post_data,"post");
        promise.then(function(data){
          $scope.AlertMessage = true;
          $scope.message="Updated Successfully";
          $ionicLoading.hide();
          $timeout(function() {
            $scope.AlertMessage = false;
          }, 3000);
        });
}

/* Favourite shops
########################################################
--------------------------------------------------------*/
$scope.userfavshops=function(){

    var user_data=JSON.parse(localStorage.getItem('userData'));
    $rootScope.userid=user_data.user_id;
    var link = 'favshops';
    var post_data ={user_id:$rootScope.userid};
   // console.log(post_data);
    WebService.show_loading();
    var promise = WebService.send_data(link,post_data,"post");
        promise.then(function(data){
          console.log(data);
          if(data.fav!=""){
          $rootScope.msgfav=false;
          $rootScope.favshops=data.fav;
        }else{
          $rootScope.msgfav=true;
          $rootScope.msgf="Sorry, No Result Found.";
        }
          $state.go('userfavshops');
          $ionicLoading.hide();
        });

}


/* LOG OUT
########################################################
--------------------------------------------------------*/
$scope.logout = function(){
    localStorage.removeItem('userData');

    /* // NIK
    localStorage.removeItem('city');
    var user_status = { "status": "signedout" };
    localStorage.setItem('userStatus', JSON.stringify(user_status));
    //NIK END*/

    WebService.show_loading();
    $rootScope.loginstatus="";
    $timeout(function(){
        $ionicLoading.hide();
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: true
        });
        window.location.reload();
       /* $state.go('menu.home', null, {reload: true});
    }, 1000);*/

        $rootScope.logout=false;
        $rootScope.signin=true;
        $rootScope.logs=false;
        $rootScope.notlog=true;
        $state.go('menu.home', {}, {reload: true});
    }, 1000);

  };

  /*$rootScope.do_logout = function () {
    localStorage.removeItem('userData');
      $state.go("signin",null,{reload:true});
      window.localStorage.clear();
    };*/




    /* Get single shop details from id
########################################################
--------------------------------------------------------*/
 //$rootScope.favid="";
  $scope.viewShopDetails=function(shop_id){

   // delete $rootScope.shopdata;
    var user_data=JSON.parse(localStorage.getItem('userData'));
    if(user_data!=null){
      $scope.userid=user_data.user_id;
      var post_data = {id:shop_id,user_id:$scope.userid};
    }else{
      var post_data = {id:shop_id};
    }
    var link = 'getsingleshop';
    //WebService.show_loading();
    var promise = WebService.send_data(link,post_data,"post");
      promise.then(function(data){

         if(data.data[0].favid==null){
            $rootScope.favid="";
            $rootScope.favid ="fav";
          }else{
            $rootScope.favid="";
            $rootScope.favid ="noFav";
          }
          $rootScope.shopdata =data.data;
          var svc =data.data[0].services.split("<=>");
          $rootScope.bkngservcs=data.data[0].services.split("<=>");
          $rootScope.services = [];
          angular.forEach(svc, function(value) {
            $rootScope.services.push(value.split("<#>"));
          });
          $rootScope.gallery=data.data[0].gallery.split("<=>");
          $rootScope.reviews=data.review;
          //$ionicLoading.hide();
         // $state.go('menu.shopdetail');*/

      });
$state.go('menu.shopdetail');

  }

$scope.closeEdit=function(){
    $scope.editModal.hide();
    // window.location.reload();
}

$scope.ChangePass=function(){
    //alert("hai");
    var userdatas=$scope.val;
    var user_data=JSON.parse(localStorage.getItem('userData'));
    $rootScope.id_user=user_data.user_id;
    //alert($rootScope.id_user);
    var link = 'changepassword';
    var post_data ={id:$rootScope.id_user,userdatas:userdatas};
    WebService.show_loading();
    var promise = WebService.send_data(link,post_data,"post");
    promise.then(function(data){
      $scope.editMsg = true;
        console.log(data);
        $ionicLoading.hide();
        $scope.changepassMsg = true;
        if(data.status=='success'){
            $scope.message = data.message;
            $timeout(function() {
                // window.location.reload();
                $scope.changepasswordModal.hide();
                $scope.changepassMsg = false;
            },2000);
        }
        else {
            $scope.message = data.message;
            $timeout(function() {
                $scope.changepassMsg = false;
            }, 2000);
        }
    });
  }



//GO BACK

$scope.backHist = function() {
    $state.go('profile');
  };

});
